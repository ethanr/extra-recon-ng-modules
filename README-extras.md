### WARNING: This could corrupt your recon-ng repository, interfere with future updates, etc. Please back up your data before attempting this.
#### WARNING: This has not been tested on repos used for development.

Install Extra Modules
---------------------
    # Install recon-ng if you have not already done so
    git clone https://bitbucket.org/LaNMaSteR53/recon-ng.git
    cd recon-ng

    git remote add extra https://bitbucket.org/ethanr/extra-recon-ng-modules.git
    git pull --no-edit extra master


Update Extra Modules
--------------------
    git pull --no-edit extra master
